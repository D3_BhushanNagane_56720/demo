const { query } = require('express')

const express = require ('express')

const router = express.Router()

const utils = require('../utils')

const db = require('../db')



// Add new Employee
router.post('/add', (request, response) => {
    const { name, salary, age } = request.body
  
    const statement = `
          insert into emp
            (name,salary,age)
          values
            ( '${name}',${salary},${age})
        `
    db.execute(statement, (error, result) => {
      response.send(utils.createResult(error, result));
    })
  })


//Display all the data of employee
router.get('/:name', (request, response) => {
    const{name} = request.params

    const statement = `SELECT * FROM emp where name = '${name}'`

    db.execute(statement, (error, result) => {
            response.send(utils.createResult(error, result))
    })
})


//DELETE employee
router.delete('/delete/:id', (request, response) => {
    const{id} = request.params
    const statement = `DELETE FROM emp WHERE id = ${id}`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router;


//UPDATE employee data
router.put('/update/:name', (request, response) => {
    const {name} = request.params
    const {salary} = request.body

    const statement = `UPDATE emp SET salary = ${salary} where name = '${name}'`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})
